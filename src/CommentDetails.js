import React from "react";

const CommentDetails = writter => {
    return (
            <div>
            <a href="#" className="right floated mini ui image">
                <img alt="avatar" src={writter.avatar}/>
            </a>
            <div className="header">
                <a href="#">
                    {writter.author}
                </a>
            </div>
                <div className="meta">
                    <span className="date">Today at: {writter.time}</span>
                </div>
                <div className="description">
                    {writter.comments}
                </div>
            </div>
    );
};

export default CommentDetails;
