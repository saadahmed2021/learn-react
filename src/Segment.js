import React from "react";

const Segment = (props) => {
    return(
        <div className="ui placeholder raised segment text-center">
        {props.children}
        </div>
    );
}

export default Segment;