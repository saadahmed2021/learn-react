import React from "react";
import ReactDOM from "react-dom";
import faker from "@faker-js/faker";
import CommentDetail from "./CommentDetails";
import ApprovalCard from "./ApprovalCard";
import Segment from "./Segment";
import "./index.css";

function getCurrentTime(){
    return (new Date()).toLocaleTimeString()
}

const App = () => {
    const RightSec = "Right";
    return (
        <div>
            <div className="ui container mt-30">
                <Segment>
                    <div className="ui icon header">
                        <i className="pdf file outline icon"></i>
                        No documents are listed for this customer.
                    </div>
                    <div className="ui primary button">Add Document</div>
                </Segment>
                <Segment>
                    <div className="ui segment">
                        <p className="fadeInTop">Top</p>
                    </div>
                    <div className="ui segment">
                        <p className="fadeInRight">{RightSec}</p>
                    </div>
                    <div className="ui segment">
                        <p className="fadeInBottom">Bottom</p>
                    </div>
                    <div className="ui segment">
                        <p className="fadeInLeft">Left</p>
                    </div>
                </Segment>
            </div>
            <div className="container ui cards mt-30 flex-cc">
                <ApprovalCard accept="Yes" reject="No">
                    <div>
                        <h4 className="text red">Danger</h4>
                        You really wanna delete this post?
                    </div>
                </ApprovalCard>
                <ApprovalCard accept="Accept" reject="Reject">
                    <CommentDetail author="James" comments="Wonderful post!" time={getCurrentTime()} avatar={faker.image.avatar()}/>
                </ApprovalCard>
                <ApprovalCard accept="Accept" reject="Reject">
                    <CommentDetail author="Jack" comments="You have written a great topic!" time={getCurrentTime()} avatar={faker.image.avatar()}/>
                </ApprovalCard>
                <ApprovalCard accept="Accept" reject="Reject">
                    <CommentDetail author="John" comments="this is awesome!" time={getCurrentTime()} avatar={faker.image.avatar()}/>
                </ApprovalCard>
            </div>
        </div>
    );
}

ReactDOM.render(<App/>,document.querySelector('#root'))